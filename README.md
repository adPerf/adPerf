adPerf is an infrastructure for performance characterization of web ads.

To use adPerf. First clone the repository:

git clone https://gitlab.com/adPerf/adPerf.git

create temporary folders at the root directory for performance tests:

mkdir tmp res trace pickles 

build modified version of zbrowse (requires npm package manager):

cd util/zbrowse

npm install

make sure the installation was successful. To test the installation of zbrowse follow these instructions:
launch a headless chrome from one terminal with a given port number.
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --headless --remote-debugging-port=9223

In another terminal, in the directory util/zbrowse execute the following command

node index.js http://www.google.com 9223

After few minutes network information should be printed on std. This shows modified zbrowse is installed properly. You can close this terminal but keep the headless chrome running.

In a fresh terminal navigate to the src directory and run the following command:

python3 main.py

This crawls the websites in setup_files/options_tiny.json . let each websites is crawled couple times and then kill the crawler (ctrl+c).
Now run the following command for measuring performance cost of ads on the crawled websites.

python3 performanceAnalyzer.py

The results should be reported in "res" directory (e.g., site_stat.json).

More documentation and code for testing other modules of adPerf will be added gradually. Meanwhile, if you have any question, you can contact bpourgha@uci.edu . If you want to contribute to adPerf or make fixes to the code, please create a PR and let us know.

Below is the link to the adPerf paper. If you plan to use adPerf or the results presented in this paper, please reference it.

adPerf: Characterizing the Performance of Third-party Ads    
Proceedings of the ACM on Measurement and Analysis of Computing SystemsFebruary 2021 Article No.: 03 https://doi.org/10.1145/3447381