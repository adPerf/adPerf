# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# adbrowser_mob.py  # # # # # # # # # # # # # # # # # # # # # # # # 
# copyright - all rights reserved # # # # # # # # # # # # # # # # #
# developers: Behnam Pourghassemi, Jordan Bonecutter  # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import filehelper
from dbprint import dbprint
import zbrowse_mob as zbrowse
import json
import treehelper
#import drawtree
import re
import subprocess
import os
import shutil
from collections import OrderedDict
from EvaluatePerformance import EvaluatePerformance
import random
import time

__chrome_path__ = "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
time_max = 220
expr_max = 2

class ParseError(RuntimeError):
    def __init__(self):
        pass

class Crawler:
    sites = None
    data  = None
    ofile = None

    def __init__(self, **kwargs):
        if "sites" in kwargs:
            self.sites = kwargs["sites"]
        else:
            self.sites = []
        #with open('setup_files/top200news.json') as fi:
        #  self.sites = json.load(fi)
        #with open('../crawler/top200news_output.json') as fi:
        #  somesites = json.load(fi)

        
        #self.sites = []
        # 
        #for dom, ss in somesites.items():
        #  if len(ss) <= 1:
        #    continue
        #  self.sites.append(ss[random.randint(1, len(ss)-1)])

        if "data" in kwargs:
            try:
                with open(kwargs["data"], "r") as fi:
                    self.data = json.load(fi)
            except FileNotFoundError:
                self.data = {}
            except json.JSONDecodeError:
                dbprint("data file " + str(kwargs["data"]) + " not a json file")
                self.data = {}
        else:
            self.data = {}

        if "ofile" in kwargs:
            self.ofile = kwargs["ofile"]
        else:
            self.ofile = "res/crawl.json"

        for site in self.sites:
            if site in self.data:
                continue
            self.data.update({site: {"snapshots": [], "timer": 45}})

    @classmethod
    def fromfile(cls, fname):
        try:
            with open(fname, "r") as fi:
                return cls(**json.load(fi))
        except json.JSONDecodeError:
            dbprint("file " + fname + " is not a json file")
            raise ParseError()

    def __str__(self):
        ret = "Crawler object:\n"
        ret += "sites = " + str(self.sites) + "\n"
        ret += "data  = " + (str(self.data))[:20] + "...\n"
        ret += "ofile = " + self.ofile 
        return ret

    def crawl(self, **kwargs):
        try:
            port = kwargs["port"]
        except KeyError:
            port = 9222

        try:
            draw = kwargs["draw"]
        except KeyError:
            draw = False

        try:
            perfFile = kwargs["perfFile"]
        except KeyError:
            perfFile = None

        try:
            #chrome = subprocess.Popen((__chrome_path__, "--headless", "--remote-debugging-port="+str(port), "--disable-gpu", "--no-sandbox", "--disk-cache-size=1", "--disable-gpu-program-cache", "--media-cache-size=1", "--aggressive-cache-discard", "--single-process", "--no-first-run", "--no-default-browser-check", "--user-data-dir=tmp/remote-profile"), stdout=open("./tmp/__chromium_stdout.log", "w"), stderr=open("./tmp/__chromium_stderr.log", "w"))
            counter = 0
            while True:
                for site in self.sites[:]:
                    if len(self.data[site]["snapshots"]) > expr_max:
                        continue
                    #if self.data[site]["timer"] == 220 or self.data[site]["timer"] == 45:
                    #    continue

                    counter += 1
                    if counter % 10 is 0:
                        self.save(self.ofile)
                    os.system("../util/platform-tools/adb shell am force-stop com.android.chrome")
                    time.sleep(1)
                    try:
                        os.system("../util/platform-tools/adb shell am start -n com.android.chrome/com.google.android.apps.chrome.Main -d www.google.com")
                        time.sleep(6)
                        os.system("../util/platform-tools/adb forward tcp:9222 localabstract:chrome_devtools_remote")
                        time.sleep(4)
                    except:
                        dbprint("Error launching Chrome")
                        continue
                    try:
                        tree, performance = zbrowse.get(domain = site, timeout = self.data[site]["timer"], port=port)
                    except zbrowse.Timeout as e:
                        dbprint("Zbrowse timed out in " + str(e.timer) + "s at site " + site) 
                        if self.data[site]["timer"]*2 < time_max:
                            self.data[site]["timer"] *= 2
                        else:
                            self.data[site]["timer"] = time_max
                    except zbrowse.OutOfMemory:
                        dbprint("ZBrowse ran out of memory at site " + site)
                    except zbrowse.IncompleteTree:
                        dbprint("Error parsing json from zbrowse at site " + site)
                    else:
                        dbprint("snapshot at " + site + " was succesful!")
                        trees = treehelper.get_tree(tree, performance)
                        dbprint("raw depth = " + str(treehelper.get_raw_depth(tree)))
                        dbprint("new depth = " + str(len(trees["tree_full"])))
                        self.data[site]["snapshots"].append(trees)
                        l = len(self.data[site]["snapshots"])
                        if perfFile:
                          site = treehelper.get_url(site)
                          try:
                            trace_path = os.path.join("../trace", site+"_"+str(l))
                            shutil.move('trace.json',trace_path)
                          except:
                            print('trace file for " + site + " is not generated or cannot be moved')
                            pass
                        if draw:
                            site = treehelper.get_url(site)
                            dbprint("drawing full")
                            drawtree.draw_tree(trees["tree_full"], "../res/img/"+site+str(l)+"full.pdf")
                            dbprint("drawing trim")
                            drawtree.draw_tree(trees["tree_trim"], "../res/img/"+site+str(l)+"trim.pdf")
                        
        except KeyboardInterrupt:
            self.save(self.ofile)
            #chrome.kill()
        raise KeyboardInterrupt()

    def save(self, fname):
        filehelper.json_save(self.data, fname)
