# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# cat_table_generator.py  # # # # # # # # # # # # # # # # # # # # #
# copyright - all rights reserved # # # # # # # # # # # # # # # # #
# developers: Behnam Pourghassemi, Jordan Bonecutter  # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def generate_table(data):
  
  for key in data['HTML'].keys():
  	data['HTML'][key] += data['Document'][key]
  	data['unknown'][key] += data['Other'][key] #+ data['Fetch'][key] 
  	data['XHR'][key] += data['Fetch'][key]
  
  data.pop('Document')
  data.pop('Other')
  data.pop('Fetch')
  
  nr_ad = 0
  nr_total = 0
  t_ad = 0.0
  t_total = 0.0
  
  for cat in data.keys():
  	nr_ad += data[cat]['ad_plus_inherited']
  	nr_total += data[cat]['total']
  	t_ad += data[cat]['network_ad_plus_inherited_total']
  	t_total += data[cat]['network_total']

  table = {}
  for cat in data.keys():
  	table[cat] = {'nr':{},'t':{}}
  	table[cat]['nr'] = {'nr_ad_c/nr_total_c':data[cat]['ad_plus_inherited']/data[cat]['total'], 'nr_ad_c/nr_ad_total':data[cat]['ad_plus_inherited']/nr_ad, 'nr_total_c/nr_total_total':data[cat]['total']/nr_total}
  	table[cat]['t'] = {'t_ad_c/t_total_c':data[cat]['network_ad_plus_inherited_total']/data[cat]['network_total'], 't_ad_c/t_ad_total':data[cat]['network_ad_plus_inherited_total']/t_ad, 't_total_c/t_total_total':data[cat]['network_total']/t_total}

  for cat in data.keys():
  	print('{0: <11}'.format(cat), table[cat]['nr'])
  print (nr_ad/nr_total)
  print (t_ad/t_total)

def main(argv) -> int:
  if len(argv) != 2:
    return 0
  
  with open(argv[1], 'r') as fi:
    data = json.load(fi)

  generate_table(data)
  #filehelper.json_save(generate_table(data), 'resource_table.json')  

  return 0

if __name__ == '__main__':
  import json
  import sys
  main(sys.argv)