# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# kbinterruptable.py  # # # # # # # # # # # # # # # # # # # # # # #
# copyright - all rights reserved # # # # # # # # # # # # # # # # #
# developers: Behnam Pourghassemi, Jordan Bonecutter  # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

from dbprint import dbprint
from typing import Callable, Any

def kbint(func: Callable, cleanup: Callable) -> Callable:
    def inner(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except KeyboardInterrupt:
            dbprint("cleaning up")
            cleanup()
        raise KeyboardInterrupt()
    return inner
