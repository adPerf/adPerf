# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# resourceAnalyzer.py # # # # # # # # # # # # # # # # # # # # # # #
# copyright - all rights reserved # # # # # # # # # # # # # # # # #
# developers: Behnam Pourghassemi, Jordan Bonecutter  # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

from treehelper import get_url as get_domain
import json
import requests
from bs4 import BeautifulSoup
import re

def calculateByType(path_to_crawl, target_site = None):
  # read the file
  with open(path_to_crawl, 'r') as fi:
    crawl = json.load(fi)
  
  # return dicts
  ret = {}
  adTagCount = []
  NoneAdTagCount = []
  siteCount = 0

  # loop through entire crawl
  for site, data in crawl.items():
    siteCount += 1
    if (target_site == None or target_site == site) and siteCount < 200:
      for snap in data['snapshots']:
        for level in snap['tree_full']:
          for url, info in level.items():
            # only do anything if current node has timing data
            if 'timing' in info and 'ad' in info and 'resptype' in info:
              if "html" in info['resptype'] or "HTML" in info ['resptype']:
                try:
                  r = requests.get(url)
                except:
                  continue
                soup = BeautifulSoup(r.text, 'html.parser')
                if info['ad'] == 'yes' or info['ad'] == 'inherited':
                  adTagCount.append(len(soup.findAll()))
                  #print("ad: " + str(len(soup.findAll())))
                  #print(soup.prettify())
                else:
                  NoneAdTagCount.append(len(soup.findAll()))
                  #print("non-ad: " + str(len(soup.findAll())))
                  #print(soup.prettify())
    else:
      continue
              
  ret['ad'] = adTagCount
  ret['non-ad'] = NoneAdTagCount

  return ret
