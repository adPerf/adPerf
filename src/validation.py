# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# validation.py # # # # # # # # # # # # # # # # # # # # # # # # # # 
# copyright - all rights reserved # # # # # # # # # # # # # # # # #
# developers: Behnam Pourghassemi, Jordan Bonecutter  # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import copy
from adblockparser import AdblockRules
import json
from json import encoder
from collections import OrderedDict
import filehelper
import os
import sys
from dbprint import dbprint
from EvaluatePerformance_compare import EvaluatePerformance
from treehelper import get_url
import random
try:
  import _pickle as pickle
except:
  import pickle


def parsePerformanceTraces(perf, trace_file_path, har_file_path):
  try:
    with open(har_file_path) as f: 
      har_data = json.load(f, object_pairs_hook=OrderedDict)
  except:
    dbprint("ERROR: Cannot Open file " + har_file)
    exit()

  _rules = None
  _rules_pkl = "../pickles/rules.pkl"
  _rules_file = "../setup_files/easylist.json"

  if os.path.exists(_rules_pkl):
    with open(_rules_pkl, "rb") as fi:
      _rules = pickle.load(fi)
  else:
    with open(_rules_file, "r") as fi:
      _rules = AdblockRules(json.loads(fi.read()))
    filehelper.file_save(_rules, _rules_pkl, pickle.dump, True)
            # set ad state


  domain = get_url(har_data["log"]["entries"][0]["request"]["url"])
  ad_urls = []
  for entry in har_data["log"]["entries"]:
    url = entry["request"]["url"]
    if _rules.should_block(url):
      ad_urls.append(url)

  try:
    trace_file = open(trace_file_path, 'r+')
    trace_data = json.load(trace_file, object_pairs_hook=OrderedDict)
  except json.decoder.JSONDecodeError: #FIXME it is seen missing "]" at the end Zbrowse trace output file
    try:  
      trace_file.write("]")
      trace_file.seek(0)
      trace_data = json.load(trace_file, object_pairs_hook=OrderedDict)
    except:
      dbprint("ERROR encoding json file. Added extra ""]"" to the end of file")
      exit()
  except:
    dbprint("ERROR: cannot open file " + trace_file)
    exit()

  trace_file.close()

  if isinstance(trace_data, dict):
    trace_data = trace_data['traceEvents']
  perf.add_performance(domain, trace_data, ad_urls)
  return


def main(argv):
  if len(argv) != 3:
    dbprint("ERROR: argument should be validation.py tracefilename harfilename")
    return
  perf = EvaluatePerformance()
  dir_path = os.path.dirname(os.path.realpath(__file__))
  trace_dir = os.path.join(dir_path,'trace')
  har_dir = os.path.join(dir_path,'har')
  parsePerformanceTraces(perf, os.path.join(trace_dir,argv[1]), os.path.join(har_dir,argv[2]))
  #perf.print_perfEvents('perf_mob_news_p3.log')
  #perf.read_perfEvents_from_file('perf_mob_news.log')
  print(perf.calculateAdPerformance(True))  

  cat_data = perf.calculateAdPerformanceByCategory(True)
  performance_brakdown = {'Scripting':[0.,0.], 'HTML Parsing':[0.,0.], 'Layout':[0.,0.], 'Paint':[0.,0.], 'Composite':[0.,0.]}
  for cat, time in cat_data.items():
    if cat == 'js_evaluate':
      ncat = 'Scripting'
    if cat == 'js_function':
      ncat = 'Scripting'
    if cat == 'html_parse':
      ncat = 'HTML Parsing'
    if cat == 'css_parse':
      ncat = 'HTML Parsing'
    if cat == 'style_recalc':
      ncat = 'Layout'
    if cat == 'layout':
      ncat = 'Layout'
    if cat == 'update_layer':
      ncat = 'Layout'
    if cat == 'paint':
      ncat = 'Paint'
    if cat == 'paint_image':
      ncat = 'Paint'
    if cat == 'composite':
      ncat = 'Paint'
    if cat == 'xhr':
      ncat = 'Scripting'
    performance_brakdown[ncat][0] += time[0]
    performance_brakdown[ncat][1] += time[1]
  print(performance_brakdown)


if __name__ == "__main__":
    main(sys.argv)
