# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# wot_classifier.py # # # # # # # # # # # # # # # # # # # # # # # # 
# copyright - all rights reserved # # # # # # # # # # # # # # # # #
# developers: Behnam Pourghassemi, Jordan Bonecutter  # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import json
import requests
from pprint import pprint
import filehelper


if __name__ == "__main__" :
  stat = {'Excellent':[], 'Good':[], 'Unsatisfactory':[], 'Poor':[], 'Very poor':[]}
  with open('addomainsWot.json', 'r') as fi:
    score = json.load(fi)
  with open('vtout.json', 'r') as fi:
    vtout = json.load(fi)
  for domain , val in vtout.items():
    if 'wot' in vtout[domain].keys():
      if 'Trustworthiness' in vtout[domain]['wot'].keys():
        if domain in score.keys():
          if score[domain] > 0:
            qual = vtout[domain]['wot']['Trustworthiness']
            stat[qual].append([score[domain],domain])

  for key,val in stat.items():
    stat[key] = sorted(val)

  filehelper.json_save(stat,'class.json')
